/** Bài 3: Quy đổi tiền
 * 
 * Đầu vào:
 * + 23.500 vnd
 * + 2 usd
 * 
 * Các bước xử lý:
 * 
 * Bước 1: Gán biến var vnd = 23500; gán biến var usd = 2;
 * Bước 2: Viết chương trình: var money = usd * vnd
 * 
 * Đầu ra:
 * In ra kết quả console.log()
 * 
*/

// Đầu vào: 
var usd = 2;
var vnd = 23500;

// Các bước xử lý
var money = usd * vnd;

// Đầu ra
console.log("Số tiền sau quy đổi =",money + "VND")