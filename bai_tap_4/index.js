// Bài 4: Tính diện tích và chu vi hình chữ nhật

// đầu vào
 var chieuDai = 30;
 var chieuRong = 20;

//  Các bước xử lý
 var dienTich = chieuDai * chieuRong;
 var chuVi = (chieuDai + chieuRong)*2;

// Đầu ra
 console.log("Diện tích hình chữ nhật là:",dienTich)
 console.log("Chu vi hình chữ nhật là:", chuVi)