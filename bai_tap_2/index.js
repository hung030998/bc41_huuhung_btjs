//Đầu vào: User nhập vào 5 số thực ngẫu nhiêng

/**
 * Các bước xử lý: 
 *  + tạo 1 function, sau đó Dom tới thẻ input, và thẻ trả kết quả
 *  + lấy giá trị trong input bằng phương thức input.value,
 *  + sau đó chuyển kiểu string sang number bằng, và cộng các giá trị lại và chi cho 5
 *  + Gán kết quả. Dùng thẻ .innerHTML để lấy giá trị trong thẻ
 *  + Đặt thẻ onclick=fuction() ở button. để user click hiển thị kết quả
 */

function mediumNumber(){
    var number1 = document.querySelector(".num1").value;
    var number2 = document.querySelector(".num2").value;
    var number3 = document.querySelector(".num3").value;
    var number4 = document.querySelector(".num4").value;
    var number5 = document.querySelector(".num5").value;
    var idresult = document.querySelector(".result span")

    var giaTriTrungBinh = (parseInt(number1) + parseInt(number2) + parseInt(number3) + parseInt(number4) + parseInt(number5)) / 5;
    idresult.innerHTML = giaTriTrungBinh;
}

// Đầu ra: Click vào button để hiển thị kết quả